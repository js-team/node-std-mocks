# Installation
> `npm install --save @types/std-mocks`

# Summary
This package contains type definitions for std-mocks (https://github.com/neoziro/std-mocks#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/std-mocks.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/std-mocks/index.d.ts)
````ts
// Type definitions for std-mocks 1.0
// Project: https://github.com/neoziro/std-mocks#readme
// Definitions by: Jeff Dickey <https://github.com/jdxcode>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

export interface Options {
    stdout?: boolean | undefined;
    stderr?: boolean | undefined;
}

/**
 * Start mocking std output
 */
export function use(opts?: Options & {print?: boolean | undefined}): void;

/**
 * Restore std output
 */
export function restore(opts?: Options): void;

/**
 * Flush collected output
 */
export function flush(opts: Options & {stdout: false, stderr: false}): {};
export function flush(opts: Options & {stderr: false}): {stdout: string[]};
export function flush(opts: Options & {stdout: false}): {stderr: string[]};
export function flush(opts?: Options): {stdout: string[], stderr: string[]};

````

### Additional Details
 * Last updated: Fri, 02 Jul 2021 22:32:54 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Jeff Dickey](https://github.com/jdxcode).
